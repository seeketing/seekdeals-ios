//
//  Deal+CoreDataProperties.swift
//  SeekDeals
//
//  Created by Arturo Rivas on 3/5/16.
//  Copyright © 2016 Seeketing SL. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

import Foundation
import CoreData

extension Deal {

    @NSManaged var categoryId: NSNumber?
    @NSManaged var desc: String?
    @NSManaged var id: NSNumber?
    @NSManaged var image: Data?
    @NSManaged var imageUrl: String?
    @NSManaged var name: String?
    @NSManaged var price: NSDecimalNumber?
    @NSManaged var dealerId: NSNumber?

}
