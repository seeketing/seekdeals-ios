//
//  Category.swift
//  SeekDeals
//
//  Created by Arturo Rivas on 15/3/16.
//  Copyright © 2016 Seeketing SL. All rights reserved.
//

import Foundation
import CoreData

let GET_CATEGORIES = "http://www.siketing.com/deals/rest/?method=getDealsCategoriesData&params={\"app_id\":APPID}"

class Category: NSManagedObject {

    class func insertNewCategory(_ managedObjectContext: NSManagedObjectContext, data: [String: AnyObject]) {
        let category = NSEntityDescription.insertNewObject(forEntityName: "Category", into: managedObjectContext) as! Category
        
        category.name = data["name"] as? String
        category.imageUrl = data["image"] as? String
        category.color = data["color"] as! String + "FF"
        category.image = try? Data(contentsOf: URL(string: IMAGES_PREV_BASE_URL + category.imageUrl!)!)
        category.id = Int(data["id"] as! String) as NSNumber?
    }
    
    class func getAllCategories(_ managedObjectContext: NSManagedObjectContext, filterEmpty: Bool = false) -> [Category] {
        let newFetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "Category")
        do {
            return try managedObjectContext.fetch(newFetchRequest) as! [Category]
        } catch {
            let nserror = error as NSError
            NSLog("Unresolved error \(nserror), \(nserror.userInfo)")
            
            return [Category]()
        }
    }
    
    class func getCategoriesFromServer(_ appId: Double) -> Bool {
        let newString = GET_CATEGORIES.replacingOccurrences(of: "APPID", with: String(appId))
        let url = URL(string: newString.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!)
        
        if let data = try? Data(contentsOf: url!) {
            if let responseData = ServerResponse.parseFromData(data) {
                if responseData.records != 0 {
                    let data = responseData.data as! [[String: AnyObject]]
                    
                    let appDelegate = UIApplication.shared.delegate as! AppDelegate
                    let managedObjectContext = appDelegate.managedObjectContext
                    let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "Category")
                    do {
                        let objects = try managedObjectContext?.fetch(fetchRequest) as! [Category]
                        
                        for object in objects {
                            managedObjectContext?.delete(object)
                        }
                    } catch {
                        let nserror = error as NSError
                        NSLog("Unresolved error \(nserror), \(nserror.userInfo)")
                        
                        return false
                    }
                    
                    for category in data {
                        insertNewCategory(appDelegate.managedObjectContext!, data: category)
                    }
                    
                    return true
                }
            }
        }
        
        return false
    }

}
