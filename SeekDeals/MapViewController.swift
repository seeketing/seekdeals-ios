//
//  MapViewController.swift
//  SeekDeals
//
//  Created by Arturo Rivas on 24/2/16.
//  Copyright © 2016 Seeketing SL. All rights reserved.
//

import UIKit
import MapKit

class MapViewController: UIViewController, MKMapViewDelegate {

    var appDelegate = UIApplication.shared.delegate as! AppDelegate
    let dataUser = AppIdData()
    
    var dealers: [Dealer]?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationItem.title = dataUser.name
        
        dealers = Dealer.getAllDealers(appDelegate.managedObjectContext!)
        
        centerMap()
        addMapAnnotations()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        SETS.trackEventNavi("Mapa", withPercent: 20, andInfo: nil)
    }
    
    // MARK: - Functions
    
    func centerMap() {
        let coordinates = CLLocationCoordinate2DMake(40.4378698, -3.8196207)
        let region = MKCoordinateRegionMakeWithDistance(coordinates, 120000, 120000)
        
        let mapView = self.view as! MKMapView
        mapView.centerCoordinate = coordinates
        mapView.setRegion(region, animated: true)
    }
    
    func addMapAnnotations() {
        var annotations = [MKPointAnnotationDeal]()
        
        for dealer in dealers! {
            let annotation = MKPointAnnotationDeal()
            annotation.title = dealer.name
            annotation.subtitle = dealer.desc
            annotation.coordinate = CLLocationCoordinate2DMake(Double(dealer.latitude!), Double(dealer.longitude!))
            annotation.dealer = dealer
            
            annotations.append(annotation)
        }
        
        let mapView = self.view as! MKMapView
        mapView.addAnnotations(annotations)
    }
    
    // MARK: - MapView Delegate
    
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        
        if annotation is MKUserLocation {
            return nil
        }
        
        let pinView = MKPinAnnotationView()
        pinView.annotation = annotation
        if #available(iOS 9.0, *) {
            pinView.pinTintColor = UIColor.red
        }
        pinView.animatesDrop = true
        pinView.canShowCallout = true
        
        let calloutButton = UIButton(type: .detailDisclosure)
        calloutButton.tintColor = UIColor.black
        pinView.rightCalloutAccessoryView = calloutButton
        pinView.calloutOffset = CGPoint(x: -10,y: 0)
        
        return pinView
    }
    
    func mapView(_ mapView: MKMapView, annotationView view: MKAnnotationView, calloutAccessoryControlTapped control: UIControl) {
        if !view.isKind(of: MKUserLocation.self) {
            let dealAnnotation = view.annotation as! MKPointAnnotationDeal
            
            SETS.trackEventClick(dealAnnotation.dealer.name!)
            
            mapView.deselectAnnotation(view.annotation, animated: false)
            performSegue(withIdentifier: "dealerDeals", sender: dealAnnotation.dealer)
        }
    }
    
    // MARK: - Segues
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "dealerDeals" {
            let dealer = sender as! Dealer
            let vc = segue.destination as! DealerDealsTableViewController
            vc.detailItem = dealer
        }
    }

}

class MKPointAnnotationDeal: MKPointAnnotation {
    var dealer: Dealer!
}
