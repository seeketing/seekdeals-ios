//
//  Category+CoreDataProperties.swift
//  SeekDeals
//
//  Created by Arturo Rivas on 15/3/16.
//  Copyright © 2016 Seeketing SL. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

import Foundation
import CoreData

extension Category {

    @NSManaged var name: String?
    @NSManaged var image: Data?
    @NSManaged var imageUrl: String?
    @NSManaged var color: String?
    @NSManaged var id: NSNumber?
}
