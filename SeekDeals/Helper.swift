//
//  Helper.swift
//  SeeketingMupis
//
//  Created by Arturo Rivas on 4/2/16.
//  Copyright © 2016 Arturo Rivas. All rights reserved.
//

import UIKit

class Helper {

    class func showActivityIndicatory(_ container: UIView) -> UIView {
        let loadingView: UIView = UIView()
        loadingView.frame = CGRect(x: 0, y: 0, width: 80, height: 80)
        let screenSize = UIScreen.main.bounds
        loadingView.center = CGPoint(x: screenSize.width / 2, y: screenSize.height / 2 - 40)
        loadingView.backgroundColor = UIColor.black
        loadingView.clipsToBounds = true
        loadingView.layer.cornerRadius = 10
        
        let actInd: UIActivityIndicatorView = UIActivityIndicatorView()
        actInd.frame = CGRect(x: 0, y: 0, width: 40, height: 40);
        actInd.activityIndicatorViewStyle = .whiteLarge
        actInd.center = CGPoint(x: loadingView.frame.size.width / 2, y: loadingView.frame.size.height / 2)
        loadingView.addSubview(actInd)
        container.addSubview(loadingView)
        actInd.startAnimating()
        
        return loadingView
    }
    
    class func changeAppColors(_ appData: AppIdData) {
        let backgroundColor = UIColor(hexString: appData.backgroundColor!)
        let textColor = UIColor(hexString: appData.textColor!)
        
        UITabBar.appearance().tintColor = textColor
        UITabBar.appearance().barTintColor = backgroundColor
        
        UINavigationBar.appearance().titleTextAttributes = [NSForegroundColorAttributeName: textColor]
        UINavigationBar.appearance().barTintColor = backgroundColor
        UINavigationBar.appearance().tintColor = textColor
        
        UIBarButtonItem.appearance().tintColor = textColor
    }
    
    class func showAlert(_ backgroundColor: UIColor, textColor: UIColor, message: String)
    {
        
        let appDelegate: AppDelegate = UIApplication.shared.delegate as! AppDelegate
        let label = UILabel(frame: CGRect.zero)
        label.textAlignment = NSTextAlignment.center
        label.text = message
        //label.font = UIFont(name: NEWFONT_NAVIGATIONBAR, size: NEWFONT_NAVIGATIONBAR_SIZE)
        label.adjustsFontSizeToFitWidth = true
        
        label.backgroundColor =  backgroundColor //UIColor.whiteColor()
        label.textColor = textColor //TEXT COLOR
        
        label.sizeToFit()
        label.numberOfLines = 4
        label.layer.shadowColor = UIColor.gray.cgColor
        label.layer.shadowOffset = CGSize(width: 4, height: 3)
        label.layer.shadowOpacity = 0.3
        label.frame = CGRect(x: 320, y: 64, width: appDelegate.window!.frame.size.width, height: 44)
        label.alpha = 1
        
        appDelegate.window!.addSubview(label)
        
        var basketTopFrame: CGRect = label.frame;
        basketTopFrame.origin.x = 0;
        
        UIView.animate(withDuration: 2.0, delay: 0.0, usingSpringWithDamping: 0.5, initialSpringVelocity: 0.1, options: UIViewAnimationOptions.curveEaseOut, animations: { () -> Void in
            label.frame = basketTopFrame
            },  completion: {
                (value: Bool) in
                UIView.animate(withDuration: 2.0, delay: 2.0, usingSpringWithDamping: 0.5, initialSpringVelocity: 0.1, options: UIViewAnimationOptions.curveEaseIn, animations: { () -> Void in
                    label.alpha = 0
                    },  completion: {
                        (value: Bool) in
                        label.removeFromSuperview()
                })
        })
    }
}

extension UIColor {
    public convenience init(hexString: String) {
        let r, g, b, a: CGFloat
        
        var start = hexString.startIndex
        
        if hexString.hasPrefix("#") {
            start = hexString.index(start, offsetBy: 1)
        }
        
        let hexColor = hexString.substring(from: start)
        
        if hexColor.characters.count == 8 {
            let scanner = Scanner(string: hexColor)
            var hexNumber: UInt64 = 0
            
            if scanner.scanHexInt64(&hexNumber) {
                r = CGFloat((hexNumber & 0xff000000) >> 24) / 255
                g = CGFloat((hexNumber & 0x00ff0000) >> 16) / 255
                b = CGFloat((hexNumber & 0x0000ff00) >> 8) / 255
                a = CGFloat(hexNumber & 0x000000ff) / 255
                
                self.init(red: r, green: g, blue: b, alpha: a)
                return
            }
        }
        
        self.init(red: 1, green: 1, blue: 1, alpha: 1)
    }
}
