//
//  Dealer.swift
//  SeekDeals
//
//  Created by Arturo Rivas on 14/3/16.
//  Copyright © 2016 Seeketing SL. All rights reserved.
//

import Foundation
import CoreData

let GET_DEALERS = "http://www.siketing.com/deals/rest/?method=getDealersData&params={\"app_id\":APPID}"

class Dealer: NSManagedObject {

    class func insertNewDealer(_ managedObjectContext: NSManagedObjectContext, data: [String: AnyObject]) {
        let dealer = NSEntityDescription.insertNewObject(forEntityName: "Dealer", into: managedObjectContext) as! Dealer
        
        dealer.name = data["name"] as? String
        dealer.desc = data["description"] as? String
        dealer.longitude = Float(data["longitude"] as! String) as NSNumber?
        dealer.latitude = Float(data["latitude"] as! String) as NSNumber?
        dealer.id = Int(data["id"] as! String) as NSNumber?
    }
    
    class func getAllDealers(_ managedObjectContext: NSManagedObjectContext) -> [Dealer] {
        let newFetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "Dealer")
        do {
            return try managedObjectContext.fetch(newFetchRequest) as! [Dealer]
        } catch {
            let nserror = error as NSError
            NSLog("Unresolved error \(nserror), \(nserror.userInfo)")
            
            return [Dealer]()
        }
    }
    
    class func getDealersFromServer(_ appId: Double) -> Bool {
        let newString = GET_DEALERS.replacingOccurrences(of: "APPID", with: String(appId))
        let url = URL(string: newString.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!)
        
        if let data = try? Data(contentsOf: url!) {
            if let responseData = ServerResponse.parseFromData(data) {
                if responseData.records != 0 {
                    let data = responseData.data as! [[String: AnyObject]]
                    
                    let appDelegate = UIApplication.shared.delegate as! AppDelegate
                    let managedObjectContext = appDelegate.managedObjectContext
                    let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "Dealer")
                    do {
                        let objects = try managedObjectContext?.fetch(fetchRequest) as! [Dealer]
                        
                        for object in objects {
                            managedObjectContext?.delete(object)
                        }
                    } catch {
                        let nserror = error as NSError
                        NSLog("Unresolved error \(nserror), \(nserror.userInfo)")
                        
                        return false
                    }
                    
                    for dealer in data {
                        insertNewDealer(appDelegate.managedObjectContext!, data: dealer)
                    }
                    
                    return true
                }
            }
        }
        
        return false
    }

}
