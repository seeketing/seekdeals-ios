//
//  Deal.swift
//  SeekDeals
//
//  Created by Arturo Rivas on 22/2/16.
//  Copyright © 2016 Seeketing SL. All rights reserved.
//

import Foundation
import CoreData

let GET_DEALS = "http://siketing.com/deals/rest?method=getDealsData&params={\"app_id\":APPID}"

class Deal: NSManagedObject {
    
    class func insertNewDeal(_ managedObjectContext: NSManagedObjectContext, data: [String: AnyObject]) {
        let deal = NSEntityDescription.insertNewObject(forEntityName: "Deal", into: managedObjectContext) as! Deal
        
        deal.name = data["name"] as? String
        deal.desc = data["description"] as? String
        deal.id = Int(data["id"] as! String) as NSNumber?
        deal.imageUrl = data["image"] as? String
        deal.image = try? Data(contentsOf: URL(string: IMAGES_PREV_BASE_URL + deal.imageUrl!)!)
        deal.price = NSDecimalNumber(string: (data["deal_price"] as! String))
        deal.categoryId = Int(data["category_id"] as! String) as NSNumber?
        deal.dealerId = Int(data["dealer_id"] as! String) as NSNumber?
    }
    
    class func getAllDeals(_ managedObjectContext: NSManagedObjectContext) -> [Deal] {
        let newFetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "Deal")
        do {
            return try managedObjectContext.fetch(newFetchRequest) as! [Deal]
        } catch {
            let nserror = error as NSError
            NSLog("Unresolved error \(nserror), \(nserror.userInfo)")
            
            return [Deal]()
        }
    }
    
    class func getDealsForCategory(_ managedObjectContext: NSManagedObjectContext, filterEmpty: Bool = false) -> [[Deal]] {
        var dealsCategorized = [[Deal]]()
        
        let categories = Category.getAllCategories(managedObjectContext)
        
        for category in categories {
            let newFetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "Deal")
            newFetchRequest.predicate = NSPredicate(format: "categoryId = %@", category.id!)
            do {
                let dealArray = try managedObjectContext.fetch(newFetchRequest) as! [Deal]
                dealsCategorized.append(dealArray)
            } catch {
                let nserror = error as NSError
                NSLog("Unresolved error \(nserror), \(nserror.userInfo)")
            }
        }
        
        return filterEmpty ? filterEmptyCategories(dealsCategorized) : dealsCategorized
    }
    
    class func getDealsForDealer(_ managedObjectContext: NSManagedObjectContext, filterEmpty: Bool = false, id: NSNumber) -> [[Deal]] {
        var dealsCategorized = [[Deal]]()
        
        let categories = Category.getAllCategories(managedObjectContext)
        
        for category in categories {
            let newFetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "Deal")
            let pred1 = NSPredicate(format: "dealerId = %@", id)
            let pred2 = NSPredicate(format: "categoryId = %@", category.id!)
            newFetchRequest.predicate = NSCompoundPredicate(type: .and, subpredicates: [pred1, pred2])
            
            do {
                let dealArray = try managedObjectContext.fetch(newFetchRequest) as! [Deal]
                dealsCategorized.append(dealArray)
            } catch {
                let nserror = error as NSError
                NSLog("Unresolved error \(nserror), \(nserror.userInfo)")
            }
        }
        
        return filterEmpty ? filterEmptyCategories(dealsCategorized) : dealsCategorized
    }
    
    fileprivate class func filterEmptyCategories(_ deals: [[Deal]]) -> [[Deal]] {
        var newDeals = deals
        for (index, categories) in newDeals.enumerated() {
            if categories.count == 0 {
                newDeals.remove(at: index)
            }
        }
        
        return newDeals
    }

    class func getDealsFromServer(_ appId: Double) -> Bool {
        let newString = GET_DEALS.replacingOccurrences(of: "APPID", with: String(appId))
        let url = URL(string: newString.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!)
        
        if let data = try? Data(contentsOf: url!) {
            if let responseData = ServerResponse.parseFromData(data) {
                if responseData.records != 0 {
                    let data = responseData.data as! [[String: AnyObject]]
                    
                    let appDelegate = UIApplication.shared.delegate as! AppDelegate
                    let managedObjectContext = appDelegate.managedObjectContext
                    let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "Deal")
                    do {
                        let objects = try managedObjectContext?.fetch(fetchRequest) as! [Deal]
                        
                        for object in objects {
                            managedObjectContext?.delete(object)
                        }
                    } catch {
                        let nserror = error as NSError
                        NSLog("Unresolved error \(nserror), \(nserror.userInfo)")
                        
                        return false
                    }

                    for deal in data {
                        insertNewDeal(appDelegate.managedObjectContext!, data: deal)
                    }
                                        
                    return true
                }
            }
        }
        
        return false
    }
}
