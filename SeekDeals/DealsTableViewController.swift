//
//  DealsTableViewController.swift
//  SeekDeals
//
//  Created by Arturo Rivas on 22/2/16.
//  Copyright © 2016 Seeketing SL. All rights reserved.
//

import UIKit

class DealsTableViewController: UITableViewController {

    var appDelegate = UIApplication.shared.delegate as! AppDelegate
    let dataUser = AppIdData()
    
    var dealsCategorized = [[Deal]]()
    var categories = [Category]()
    
    var dealsCategorizedFiltered = [[Deal]]()
    
    let searchController = UISearchController(searchResultsController: nil)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationItem.title = dataUser.name
        
        dealsCategorized = Deal.getDealsForCategory(appDelegate.managedObjectContext!)
        dealsCategorizedFiltered = dealsCategorized
        categories = Category.getAllCategories(appDelegate.managedObjectContext!)
        
        tableView.tableFooterView = UIView(frame: CGRect.zero)
        
        searchController.searchResultsUpdater = self
        searchController.dimsBackgroundDuringPresentation = false
        definesPresentationContext = true
        tableView.tableHeaderView = searchController.searchBar
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        SETS.trackEventNavi("Listado", withPercent: 10, andInfo: nil)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Segues
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showDetail" {
            let deal = sender as! Deal
            let vc = segue.destination as! DetailTableViewController
            vc.deal = deal
        }
    }
    
    // MARK: UISearchController
    
    func filterContentForSearchText(_ searchText: String, scope: String = "All") {
        for (index, dealsCategory) in dealsCategorized.enumerated() {
            dealsCategorizedFiltered[index] = dealsCategory.filter { deal in
                return deal.name!.lowercased().contains(searchText.lowercased())
            }
        }
        
        tableView.reloadData()
    }
    
    // MARK: - TableView

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if searchController.isActive && searchController.searchBar.text != "" {
            return dealsCategorizedFiltered[section].count
        }
        return dealsCategorized[section].count
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return dealsCategorized.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cellDeals", for: indexPath) as! DealTableViewCell
        
        let deal: Deal
        if searchController.isActive && searchController.searchBar.text != "" {
            deal = dealsCategorizedFiltered[indexPath.section][indexPath.row]
        } else {
            deal = dealsCategorized[indexPath.section][indexPath.row]
        }
        
        cell.name.text = deal.name
        cell.details.text = deal.desc
        cell.imageView?.image = UIImage(data: deal.image! as Data)
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)

        let deal: Deal
        if searchController.isActive && searchController.searchBar.text != "" {
            deal = dealsCategorizedFiltered[indexPath.section][indexPath.row]
        } else {
            deal = dealsCategorized[indexPath.section][indexPath.row]
        }
        
        performSegue(withIdentifier: "showDetail", sender: deal)
    }
    
    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return categories[section].name
    }
    
    override func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int) {
        let header = view as! UITableViewHeaderFooterView
        header.contentView.backgroundColor = UIColor(hexString: categories[section].color!)
        header.textLabel!.text = categories[section].name
        header.textLabel!.textColor = UIColor.white
    }
}

extension DealsTableViewController: UISearchResultsUpdating {
    func updateSearchResults(for searchController: UISearchController) {
        filterContentForSearchText(searchController.searchBar.text!)
    }
}
