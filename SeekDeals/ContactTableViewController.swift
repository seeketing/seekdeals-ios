//
//  ContactTableViewController.swift
//  SeekDeals
//
//  Created by Arturo Rivas on 29/2/16.
//  Copyright © 2016 Seeketing SL. All rights reserved.
//

import UIKit
import MessageUI

class ContactTableViewController: UITableViewController, MFMailComposeViewControllerDelegate {

    var appDelegate = UIApplication.shared.delegate as! AppDelegate
    let dataUser = AppIdData()
    
    let textos = [NSLocalizedString("Name", comment: ""), NSLocalizedString("Surname", comment: ""), NSLocalizedString("Phone", comment: ""), NSLocalizedString("E-mail", comment: "")]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationItem.title = dataUser.name
        
        tableView.estimatedRowHeight = 80
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.setNeedsLayout()
        tableView.layoutIfNeeded()
        
        tableView.tableFooterView = UIView(frame: CGRect.zero)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        SETS.trackEventNavi("Contacto", withPercent: 30, andInfo: nil)
    }
    
    @IBAction func closeKeyboard(_ sender: AnyObject) {
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 6
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == 0 {
            return tableView.dequeueReusableCell(withIdentifier: "instrucsCell")!
        } else if indexPath.row == 5 {
            return tableView.dequeueReusableCell(withIdentifier: "sendCell")!
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "formCell") as! FormTableViewCell
            
            cell.fieldTextField.placeholder = textos[indexPath.row - 1]
            
            return cell
        }
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        if indexPath.row == 5 {
            var textos = [String]()
            for row in 1...4 {
                let cell = tableView.cellForRow(at: IndexPath(row: row, section: 0)) as! FormTableViewCell
                textos.append(cell.fieldTextField.text!)
            }
            sendMailToSeeketing(textos)
        }
    }
    
    func sendMailToSeeketing(_ datos: [String]) {
        if MFMailComposeViewController.canSendMail() {
            let mail = MFMailComposeViewController()
            mail.mailComposeDelegate = self
            mail.setToRecipients(["contact@seeketing.com"])
            mail.setSubject(NSLocalizedString("Contact request", comment: ""))
            let body = String(format: NSLocalizedString("%@ %@ needs more information about Seeketing SL.\nPhone: %@\nE-Mail: %@", comment: ""), datos [0], datos[1], datos[2], datos[3])
            mail.setMessageBody(body, isHTML: false)
            
            present(mail, animated: true, completion: nil)
        } else {
            let newAlert = UIAlertController(title: "Error", message: NSLocalizedString("You need to configurate your Mail app with a valid account", comment: ""), preferredStyle: .alert)
            newAlert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
            self.present(newAlert, animated: true, completion: nil)
        }
    }
    
    // MARK: - MFMailComposeViewController Delegate
    
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        controller.dismiss(animated: true, completion: nil)
    }
}
