//
//  AppIdData.swift
//  SeekDeals
//
//  Created by Arturo Rivas on 11/3/16.
//  Copyright © 2016 Seeketing SL. All rights reserved.
//

import Foundation

let DATAUSER_URL = "http://siketing.com/deals/rest/?method=getClientDataFromActivationCode&params={\"activation_code\":\"DEMO\"}"
let IMAGES_PREV_BASE_URL = "http://www.siketing.com/deals/public/media/images/"

class AppIdData {
    
    fileprivate let defaults = UserDefaults.standard
    
    fileprivate(set) var appId: Double
    fileprivate(set) var name: String?
    fileprivate(set) var backgroundColor: String?
    fileprivate(set) var textColor: String?
    fileprivate(set) var logo: Data?
    
    var configured: Bool
    
    init() {
        appId = defaults.double(forKey: "app_id")
        name = defaults.string(forKey: "name")
        
        logo = defaults.object(forKey: "logo") as? Data
        
        backgroundColor = defaults.string(forKey: "bkg_color")
        textColor = defaults.string(forKey: "text_color")
        
        configured = defaults.bool(forKey: "configured")
    }
    
    fileprivate func storePersistentData(_ appData: [String: AnyObject]) {
        appId = Double(appData["app_id"] as! String)!
        defaults.set(appId, forKey: "app_id")
        
        name = appData["name"] as? String
        defaults.set(name, forKey: "name")
        
        backgroundColor = (appData["bkg_color"] as! String) + "FF"
        defaults.set(backgroundColor!, forKey: "bkg_color")
        
        textColor = (appData["text_color"] as! String) + "FF"
        defaults.set(textColor!, forKey: "text_color")
        
        let urlLogo = appData["logo"] as? String
        logo = try? Data(contentsOf: URL(string: IMAGES_PREV_BASE_URL + urlLogo!)!)
        defaults.set(logo, forKey: "logo")
        
        defaults.set(true, forKey: "configured")
    }
    
    func getUserData(_ code: String) -> Bool {
        let newString = DATAUSER_URL.replacingOccurrences(of: "DEMO", with: code)
        let url = URL(string: newString.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!)
        
        if let data = try? Data(contentsOf: url!) {
            if let responseData = ServerResponse.parseFromData(data) {
                if responseData.records != 0 {
                    let data = responseData.data as! [[String: AnyObject]]
                    storePersistentData(data.first!)
                    
                    return true
                } else {
                    return false
                }
            }
        }
        
        return false
    }
}
