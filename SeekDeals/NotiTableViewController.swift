//
//  NotiTableViewController.swift
//  SeekDeals
//
//  Created by Arturo Rivas on 25/2/16.
//  Copyright © 2016 Seeketing SL. All rights reserved.
//

import UIKit

class NotiTableViewController: UITableViewController {

    @IBOutlet weak var cancelButton: UIBarButtonItem!
    
    var notis: [REFSElement]?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        refreshControl?.addTarget(self, action: #selector(self.handleRefresh(_:)), for: UIControlEvents.valueChanged)
        
        tableView.estimatedRowHeight = 80
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.setNeedsLayout()
        tableView.layoutIfNeeded()
        
        tableView.tableFooterView = UIView(frame: CGRect.zero)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        loadData()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        SETS.trackEventNavi("Notis", withPercent: 60, andInfo: nil)
    }
    
    // MARK: - Helpers
    
    func getDictionaryFromJsonString(_ jsonString: String, objectForKey key: String) -> AnyObject {
        let data = jsonString.data(using: String.Encoding.utf8)
        do {
            let jsonObject = try JSONSerialization.jsonObject(with: data!, options: .mutableContainers) as! [String : AnyObject]
            
            return jsonObject[key]!
        } catch let myJSONError {
            print(myJSONError)
        }
        
        return "" as AnyObject
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return notis?.count ?? 0
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "notiCell") as! NotiTableViewCell
        
        let elementREFS = notis![indexPath.row]
        
        cell.titleLabel.text = getDictionaryFromJsonString(elementREFS.params, objectForKey: "wndw_title") as? String
        cell.bodyLabel.text = getDictionaryFromJsonString(elementREFS.params, objectForKey: "wndw_msg") as? String
        
        cell.layoutMargins = UIEdgeInsets.zero
        
        return cell
    }
    
    // MARK: - TableView Delegate
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        let elementREFS = notis![indexPath.row]
        let windowNid = getDictionaryFromJsonString(elementREFS.params, objectForKey: "wndw_nid") as? Int
        
        if windowNid != nil {
            REFS.elementSelected(Int64(windowNid!))
        } else {
            UIAlertView(title: "Error", message: NSLocalizedString("Offer is not available", comment: ""), delegate: nil, cancelButtonTitle: NSLocalizedString("OK", comment: "")).show()
        }

        SETS.trackEventNavi("Promo", withPercent: 80, andInfo: nil)
        SETS.trackEventMedia("Oferta", withUrl: getDictionaryFromJsonString(elementREFS.params, objectForKey: "wndw_url") as! String, mediaId: "\(windowNid!)", mediaType: "landing", andInfo: nil)
    }
    
    // MARK: - Functions
    
    func handleRefresh(_ refreshControl: UIRefreshControl) {
        DispatchQueue.global(priority: DispatchQueue.GlobalQueuePriority.background).async(execute: {
            self.notis = REFS.windowElements() as? [REFSElement]
            
            guard self.notis?.count != 0 else {
                DispatchQueue.main.async(execute: {
                    let labelView = UILabel(frame: CGRect(x: 0, y: 0, width: self.tableView.bounds.width, height: self.tableView.bounds.height))
                    labelView.textAlignment = .center
                    labelView.text = NSLocalizedString("Have not been received notifications yet", comment: "")
                    labelView.numberOfLines = 0
                    self.tableView.backgroundView = labelView
                    
                    self.tableView.reloadData()
                    
                    refreshControl.endRefreshing()
                })
                return
            }
            
            DispatchQueue.main.async(execute: {
                self.tableView.backgroundView = nil
                self.tableView.reloadData()
                
                refreshControl.endRefreshing()
            })
        })
    }
    
    func loadData() {
        let loading = Helper.showActivityIndicatory(self.view)
        
        DispatchQueue.global(priority: DispatchQueue.GlobalQueuePriority.background).async(execute: {
            self.notis = REFS.windowElements() as? [REFSElement]
            
            guard self.notis?.count != 0 else {
                DispatchQueue.main.async(execute: {
                    let labelView = UILabel(frame: CGRect(x: 0, y: 0, width: self.tableView.bounds.width, height: self.tableView.bounds.height))
                    labelView.textAlignment = .center
                    labelView.text = NSLocalizedString("Have not been received notifications yet", comment: "")
                    labelView.numberOfLines = 0
                    self.tableView.backgroundView = labelView
                    
                    self.tableView.reloadData()
                    
                    loading.removeFromSuperview()
                })
                return
            }
            
            DispatchQueue.main.async(execute: {
                self.tableView.backgroundView = nil
                self.tableView.reloadData()
                
                loading.removeFromSuperview()
            })
        })
    }
    
    // MARK: - Actions

    @IBAction func cancelButtonPushed(_ sender: AnyObject) {
        dismiss(animated: true, completion: nil)
    }
}
