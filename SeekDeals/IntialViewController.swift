//
//  IntialViewController.swift
//  SeekDeals
//
//  Created by Arturo Rivas on 11/3/16.
//  Copyright © 2016 Seeketing SL. All rights reserved.
//

import UIKit

class IntialViewController: UIViewController, URLSessionDataDelegate {
   
    @IBOutlet weak var logoImage: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var progressView: UIProgressView!
    @IBOutlet weak var loadLabel: UILabel!
    
    var dataUser = AppIdData()
    
    var counter = 0
    
    var timer: Timer!
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        if !dataUser.configured {
            showFisrtAlert()
        } else {
            UIApplication.shared.isNetworkActivityIndicatorVisible = true
            timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(self.loadDataFromServer), userInfo: nil, repeats: true)
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if dataUser.configured {
            configureViewColors()
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Functions
    
    func showFisrtAlert() {
        let alert = UIAlertController(title: NSLocalizedString("Welcome", comment: ""), message: NSLocalizedString("To configurate the application, please introduce your code", comment: ""), preferredStyle: .alert)
        alert.addTextField(configurationHandler: { (textField) in
            textField.placeholder = NSLocalizedString("Login", comment: "")
        })
        alert.addTextField { (textField) in
            textField.placeholder = NSLocalizedString("Password", comment: "")
            textField.isSecureTextEntry = true
        }
        
        alert.addAction(UIAlertAction(title: NSLocalizedString("Confirm", comment: ""), style: .default, handler: { (action) -> Void in
            let textField = alert.textFields![0] as UITextField
            
            UIApplication.shared.isNetworkActivityIndicatorVisible = true
            let validCode = self.dataUser.getUserData(textField.text!)
            UIApplication.shared.isNetworkActivityIndicatorVisible = false
            
            if validCode {
                self.configureViewColors()
                UIApplication.shared.isNetworkActivityIndicatorVisible = true
                self.timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(self.loadDataFromServer), userInfo: nil, repeats: true)
            } else {
                let newAlert = UIAlertController(title: "Error", message: NSLocalizedString("Your code is not valid, please rewrite the code or contact with Seeketing SL", comment: ""), preferredStyle: .alert)
                newAlert.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: ""), style: .cancel, handler: { (action) -> Void in
                    self.present(alert, animated: true, completion: nil)
                }))
                self.present(newAlert, animated: true, completion: nil)
            }
        }))
        
        alert.addAction(UIAlertAction(title: NSLocalizedString("I have no code", comment: ""), style: .default, handler: { (action) -> Void in
            let url = URL(string: "mailto://contact@seeketing.com")
            UIApplication.shared.openURL(url!)
        }))
        
        present(alert, animated: true, completion: nil)
    }
    
    func loadDataFromServer() {
        
        switch counter {
        case 0:
            Category.getCategoriesFromServer(dataUser.appId)
            progressView.progress += 0.33
            loadLabel.text = NSLocalizedString("Loading dealers...", comment: "")
        case 1:
            Dealer.getDealersFromServer(dataUser.appId)
            progressView.progress += 0.33
            loadLabel.text = NSLocalizedString("Loading deals...", comment: "")
        case 2:
            Deal.getDealsFromServer(dataUser.appId)
            progressView.progress = 1
            loadLabel.text = NSLocalizedString("Completed!", comment: "")
        default:
            timer.invalidate()
            UIApplication.shared.isNetworkActivityIndicatorVisible = false
            
            let application = UIApplication.shared
            let appDelegate = application.delegate as! AppDelegate
            
            if !appDelegate.initialized {
                REFS.startSession(Int64(dataUser.appId), withLaunchOptions: nil, useLocation: true, useNotifications: true, testMode: true)
                
                let types:UIUserNotificationType = [.alert, .badge, .sound]
                let settings:UIUserNotificationSettings = UIUserNotificationSettings(types: types, categories: nil)
                application.registerUserNotificationSettings(settings)
                application.registerForRemoteNotifications()
                
                let originalSelector = #selector(REFS.receivedInternalUrl(_:andParams:))
                let swizzledSelector = #selector(AppDelegate.receivedInternalUrl(_:andParams:))
                let originalMethod = class_getInstanceMethod(object_getClass(REFS.self), originalSelector)
                let swizzledMethod = class_getInstanceMethod(object_getClass(AppDelegate.self), swizzledSelector)
                
                class_replaceMethod(object_getClass(REFS.self), swizzledSelector, method_getImplementation(swizzledMethod), method_getTypeEncoding(originalMethod))
            }
            
            let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let viewController = mainStoryboard.instantiateViewController(withIdentifier: "mainController") as! UITabBarController
            appDelegate.window?.rootViewController = viewController
        }
        
        counter += 1
    }
    
    func configureViewColors() {
        Helper.changeAppColors(dataUser)
        
        logoImage.image = UIImage(data: self.dataUser.logo! as Data)
        nameLabel.text = dataUser.name
        loadLabel.text = NSLocalizedString("Loading categories...", comment: "")
        progressView.progress = 0
        
        self.view.backgroundColor = UIColor(hexString: self.dataUser.backgroundColor!)
        nameLabel.textColor = UIColor(hexString: dataUser.textColor!)
        loadLabel.textColor = UIColor(hexString: dataUser.textColor!)
        progressView.tintColor = UIColor(hexString: dataUser.textColor!)
        
        nameLabel.isHidden = false
        logoImage.isHidden = false
        progressView.isHidden = false
        loadLabel.isHidden = false
    }
}
