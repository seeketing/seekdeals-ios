//
//  FormTableViewCell.swift
//  SeekDeals
//
//  Created by Arturo Rivas on 29/2/16.
//  Copyright © 2016 Seeketing SL. All rights reserved.
//

import UIKit

class FormTableViewCell: UITableViewCell {

    @IBOutlet weak var fieldTextField: UITextField!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
