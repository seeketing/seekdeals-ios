//
//  Dealer+CoreDataProperties.swift
//  SeekDeals
//
//  Created by Arturo Rivas on 4/5/16.
//  Copyright © 2016 Seeketing SL. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

import Foundation
import CoreData

extension Dealer {

    @NSManaged var desc: String?
    @NSManaged var latitude: NSNumber?
    @NSManaged var longitude: NSNumber?
    @NSManaged var name: String?
    @NSManaged var id: NSNumber?

}
