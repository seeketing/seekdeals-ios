//
//  DealTableViewCell.swift
//  SeekDeals
//
//  Created by Arturo Rivas on 23/2/16.
//  Copyright © 2016 Seeketing SL. All rights reserved.
//

import UIKit

class DealTableViewCell: UITableViewCell {

    @IBOutlet weak var details: UILabel!
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var preview: UIImageView!

}
