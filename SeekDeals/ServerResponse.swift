//
//  ServerResponse.swift
//  SeekDeals
//
//  Created by Arturo Rivas on 14/3/16.
//  Copyright © 2016 Seeketing SL. All rights reserved.
//

import Foundation

struct ServerResponse {
    let response: ServerResponseData
    let isError: Bool
    let errorDesc: String?
    let errorNum: String?
    let errorType: String?
    
    init(dataFromServer: [String: AnyObject]) {
        response = ServerResponseData(dataResponse: dataFromServer["response"] as! [String : AnyObject]!)
        isError = dataFromServer["isError"] as! Bool
        errorDesc = dataFromServer["errorDesc"] as? String
        errorNum = dataFromServer["errorNum"] as? String
        errorType = dataFromServer["errorType"] as? String
    }
    
    struct ServerResponseData {
        let data: AnyObject
        let records: Int
        let status: String
        
        init(dataResponse: [String: AnyObject]) {
            data = dataResponse["data"]!
            records = dataResponse["records"] as! Int
            status = dataResponse["status"] as! String
        }
    }
    
    static func parseFromData(_ data: Data) -> ServerResponse.ServerResponseData? {
        do {
            let allData = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [String: AnyObject]
            
            let response = ServerResponse(dataFromServer: allData!)
            
            if response.isError {
                NSLog("Reponse error \(response.errorNum) type \(response.errorType), \(response.errorDesc)")
                
                return nil
            }
            
            return response.response
        } catch {
            let nserror = error as NSError
            NSLog("Unresolved error \(nserror), \(nserror.userInfo)")
        }
        
        return nil
    }
}
