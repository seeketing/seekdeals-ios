//
//  DetailTableViewController.swift
//  SeekDeals
//
//  Created by Arturo Rivas on 24/2/16.
//  Copyright © 2016 Seeketing SL. All rights reserved.
//

import UIKit

class DetailTableViewController: UITableViewController {
    
    var deal: Deal!
    var appDelegate = UIApplication.shared.delegate as! AppDelegate
    
    let dataUser = AppIdData()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationItem.title = dataUser.name
        
        tableView.estimatedRowHeight = 80
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.setNeedsLayout()
        tableView.layoutIfNeeded()
        tableView.tableFooterView = UIView(frame: CGRect.zero)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        SETS.trackEventNavi("Detalle", withPercent: 50, andInfo: nil)
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 3
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case 0...2:
            return 1
        default:
            return 0
        }
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        switch indexPath.section {
        case 0:
            let cell = tableView.dequeueReusableCell(withIdentifier: "imageCell") as! ImageTableViewCell
            
            cell.imageForTable.image = UIImage(data: deal.image! as Data)
            
            SETS.trackEventMedia("Image", withUrl: deal.imageUrl!, mediaId: "\(deal.id!)", mediaType: "Imagen", andInfo: nil)
            
            cell.layoutMargins = UIEdgeInsets.zero
            
            return cell
        case 1:
            let cell = tableView.dequeueReusableCell(withIdentifier: "notiCell") as! NotiTableViewCell
            
            cell.titleLabel.text = deal.name
            cell.bodyLabel.text = deal.desc
            
            cell.layoutMargins = UIEdgeInsets.zero
            
            return cell
        case 2:
            let cell = tableView.dequeueReusableCell(withIdentifier: "precioCell")! as UITableViewCell
            
            cell.detailTextLabel!.text = "\(deal.price!) \((Locale.current as NSLocale).object(forKey: NSLocale.Key.currencySymbol)!)"
            
            cell.layoutMargins = UIEdgeInsets.zero
            
            return cell
            
        default:
            let cell = UITableViewCell()
            
            return cell
        }
    }

}
